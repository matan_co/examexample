<?php

use yii\db\Migration;

class m170718_174550_add_status_table extends Migration
{
    public function up()
    {
		$this->createTable('status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
		]);
    }

    public function down()
    {
        $this->dropTable('status');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
