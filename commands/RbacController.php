<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
		
		// add the rule
		$rule = new \app\rbac\UpdateOwnActivityRule;
		$auth->add($rule);
		
        // add "createActivity" permission
        $createActivity = $auth->createPermission('createActivity');
        $createActivity->description = 'create Activity';
        $auth->add($createActivity);
		
		// add "updateActivity" permission
        $updateActivity = $auth->createPermission('updateActivity');
        $updateActivity->description = 'update Activity';
        $auth->add($updateActivity);
		
		// add "deleteActivity" permission
        $deleteActivity = $auth->createPermission('deleteActivity');
        $deleteActivity->description = 'delete Activity';
        $auth->add($deleteActivity);
		
		// add "deleteActivity" permission
        $viewActivity = $auth->createPermission('viewActivity');
        $viewActivity->description = 'view Activity';
        $auth->add($viewActivity);
		
		// add the "updateOwnActivity" permission and associate the rule with it.
		$updateOwnActivity = $auth->createPermission('updateOwnActivity');
		$updateOwnActivity->description = 'Update own task';
		$updateOwnActivity->ruleName = $rule->name;
		$auth->add($updateOwnActivity);
		
		// add "updateUser" permission
        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'update User';
        $auth->add($updateUser);
		
		// add "createUser" permission
        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'create User';
        $auth->add($createUser);
		
		// roles
		$admin = $auth->createRole('admin');
		$auth->add($admin);
		$category_manager = $auth->createRole('category_manager');
		$auth->add($category_manager);
		$authorized = $auth->createRole('authorized');
		$auth->add($authorized);
		$not_authorized = $auth->createRole('not_authorized');
		$auth->add($not_authorized);
		
		// assign roles
		$auth->addChild($authorized, $createActivity);
		$auth->addChild($updateOwnActivity, $updateActivity);
		$auth->addChild($category_manager, $updateOwnActivity);
		$auth->addChild($admin, $updateActivity);
		$auth->addChild($admin, $updateUser);
		$auth->addChild($admin, $deleteActivity);
		
		// Hirarchy
		$auth->addChild($admin, $category_manager);
		$auth->addChild($category_manager, $authorized);
		$auth->addChild($authorized, $not_authorized);
		

    }
}